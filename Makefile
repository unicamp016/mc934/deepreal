# Directories
SRC_DIR	= src
TEST_DIR = tests
OUT_DIR = bin
REPORT_DIR = report

# Files
REPORT = report
PYTHON_FILES = $(wildcard $(SRC_DIR)/*.py $(TEST_DIR)/*.py)

.PHONY: test lint pylint pycodestyle pydocstyle build-report write-report \
	create_dirs clean

test:
	python -m pytest tests/ -vv

lint: pylint pycodestyle pydocstyle

pylint:
	@echo "\n------------------PYLINT------------------\n"
	pylint $(PYTHON_FILES) --disable=C --disable=R

pycodestyle:
	@echo "\n------------------PYCODESTYLE------------------\n"
	pycodestyle $(PYTHON_FILES) --ignore=E111

pydocstyle:
	@echo "\n------------------PYDOCSTYLE------------------\n"
	pydocstyle $(PYTHON_FILES)

write-report: $(REPORT_DIR)/$(REPORT).tex | create_dirs
	latexmk -pdf -pvc -cd $< \
		-aux-directory=$(OUT_DIR)/$(REPORT_DIR) \
		-output-directory=$(OUT_DIR)/$(REPORT_DIR)

build-report: $(REPORT_DIR)/$(REPORT).tex | create_dirs
	latexmk -pdf -cd $< \
		-aux-directory=$(OUT_DIR)/$(REPORT_DIR) \
		-output-directory=$(OUT_DIR)/$(REPORT_DIR)

$(OUT_DIR)/$(REPORT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex | create_dirs
	latexmk -pdf -cd $< \
		-aux-directory=$(OUT_DIR)/$(REPORT_DIR) \
		-output-directory=$(OUT_DIR)/$(REPORT_DIR)

create_dirs:
	@for OUTPUT in $(shell cd $(REPORT_DIR); find . -type d); do \
	  mkdir -p $(OUT_DIR)/$(REPORT_DIR)/$$OUTPUT; \
	done

$(OUT_DIR):
	@mkdir -p $(OUT_DIR)

clean:
	@find . -name "*.pyc" -delete
	@rm -rf $(OUT_DIR)/*
