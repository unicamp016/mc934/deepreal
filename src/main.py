"""Apply and visualize pre-processing steps.

These are the steps used by the developers of DADA:
  1. Apply STFT using librosa function and save as ark.
  2. Read from ark using cmvn feature.
    copy-feats ark:data/ASV19_LA/features/STFT-LPS/STFT-LPS_train-dev.ark ark:-
    apply-cmvn-sliding --center --norm-vars --cmn-window=300 --max-warnings=0
    ark:- ark:-
  3. Subtract dataset mean and standard deviation (needs to fit on the entire
    dataset after applying kaldi normalization).
  4. Uses sklearn LabelEncoder to encode string to 0-(n-1) integer
  5. Apply per-batch padding to each audio sequence using tiling with the max
    length capped at min(1500, max_batch_length)
"""
import glob
import os

import librosa
import numpy as np
import soundfile as sf
from kaldi.feat.functions import SlidingWindowCmnOptions, sliding_window_cmn
from kaldi.matrix import Matrix

from src.vis import plot_cmvn, plot_sound, plot_spectrogram

ASSETS_DIR = "assets"
SOUND_FILES = glob.glob(os.path.join(ASSETS_DIR, "*.flac"))

# STFT params
STFT_N_FFT = 512
STFT_WIN_LENGTH_MS = 25
STFT_HOP_LENGTH_MS = 10

# CMVN params
CMN_CENTER = True
CMN_NORM_VAR = True
CMN_WINDOW = 300
CMN_OPTS = SlidingWindowCmnOptions()
CMN_OPTS.center = CMN_CENTER
CMN_OPTS.cmn_window = CMN_WINDOW
CMN_OPTS.normalize_variance = CMN_NORM_VAR
CMN_OPTS.max_warnings = 0

for file in SOUND_FILES:
  data, samplerate = sf.read(file, dtype="float32")
  plot_sound(data, samplerate)

  # 1. Compute the STFT for the audio sample and create a power level
  # spectrogram.
  cur_win_length = samplerate * STFT_WIN_LENGTH_MS // 1000
  cur_hop_length = samplerate * STFT_HOP_LENGTH_MS // 1000
  Z = librosa.core.stft(
      data,
      n_fft=STFT_N_FFT,
      win_length=cur_win_length,
      hop_length=cur_hop_length)

  # The level of a sound is associated to the power (in Watts) of a sound wave.
  # We wish to capture how the level of sound is perceived by humans. Therefore,
  # we square the amplitude, since the power is proportional to the amplitude
  # squared (think P = V^2 / R), and then take the log of the result, since
  # humans perceive sound logarithmically.
  # Refer to: https://stackoverflow.com/questions/5730778/sound-spectrogram
  S = np.abs(Z)
  plot_spectrogram(S)
  S_db = np.log(S**2 + 1e-12)

  # 2. Apply Cepstral Mean and Variance Normalization (CMVN) to power
  # spectrogram. This normalizes the spectrogram to have mean 0 and variance 1
  # over a window of CMN_WINDOW frames centered on the frame we're currently
  # evaluating. In the edges of the spectrogram, modulo effects are used to
  # recover both ends of the frames.
  # Refer to: https://kaldi-asr.org/doc/transform.html
  S_db = Matrix(S_db.T)
  S_out = Matrix(*S_db.shape)
  sliding_window_cmn(CMN_OPTS, S_db, S_out)
  plot_cmvn(S_out.numpy())
