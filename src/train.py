"""Training module."""
# TODO: try different features in the input, such as mel spectrogram
# TODO: possibly change output to single neuron in order to use other metrics.
import json
import multiprocessing
import os

import fire
import tensorflow as tf
from tensorflow.keras import Input
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.metrics import CategoricalAccuracy
from tensorflow.keras.optimizers import SGD
from tensorflow.python.client import device_lib

from src.consts import MODELS
from src.data import FeatType, get_dataset
from src.metrics import (BinaryCategoricalFalseNegatives,
                         BinaryCategoricalFalsePositives,
                         BinaryCategoricalTrueNegatives,
                         BinaryCategoricalTruePositives)
from src.models import LightCNN, ResNet
from src.util import (LATEST_MODEL_NAME, get_ckpt_dir, get_log_dir,
                      get_train_config)
from src.vis import vis_batch, vis_epoch


def config_device(gpu):
  """Configure tensorflow to use device."""
  if gpu:
    gpus = tf.config.experimental.list_physical_devices("GPU")
    tf.config.experimental.set_visible_devices(gpus[0], "GPU")
    # Make sure GPU has enough memory to work with.
    tf.config.experimental.set_memory_growth(gpus[0], True)
  else:
    # Uses at most cores // 2 cores to run tensorflow.
    cores = multiprocessing.cpu_count()
    tf.config.threading.set_inter_op_parallelism_threads(cores // 4)
    tf.config.threading.set_intra_op_parallelism_threads(cores // 4)


def get_device_name(gpu):
  """Configure tensorflow for device and return its name."""
  device_list = device_lib.list_local_devices()
  device_type = "GPU" if gpu else "CPU"
  for device in device_list:
    if device.device_type == device_type:
      return device.name
  raise ValueError("No valid device found")


def train(name,
          model='lightcnn',
          feat_type=FeatType.MAG,
          feature_norm=False,
          stft_n_fft=512,
          cqt=False,
          balanced=True,
          gpu=True):

  assert model in MODELS
  config_device(gpu)

  train_ds = get_dataset(
      splits=["train"],
      feature_norm=feature_norm,
      feat_type=feat_type,
      cqt=cqt,
      stft_n_fft=stft_n_fft,
      balanced=balanced,
  )
  val_ds = get_dataset(
      splits=["dev"],
      feature_norm=feature_norm,
      feat_type=feat_type,
      cqt=cqt,
      stft_n_fft=stft_n_fft,
      balanced=False,
      shuffle=False,
  )

  os.makedirs(get_log_dir(name), exist_ok=True)
  with open(os.path.join(get_log_dir(name), "config.json"), 'w') as f:
    config = {
        "name": name,
        "model": model,
        "feat_type": FeatType(feat_type).value,
        "feature_norm": feature_norm,
        "cqt": cqt,
        "stft_n_fft": stft_n_fft,
        "balanced": balanced,
    }
    json.dump(config, f, indent=4)

  channels = 2 if feat_type == FeatType.BOTH else 1
  inputs = Input(shape=(None, stft_n_fft // 2 + 1, channels))

  model = LightCNN(inputs) if model == 'lightcnn' else ResNet(inputs)
  model.compile(
      optimizer=SGD(learning_rate=0.001, momentum=0.9),
      loss=CategoricalCrossentropy(from_logits=True),
      metrics=[
          CategoricalAccuracy(name="accuracy"),
          BinaryCategoricalFalseNegatives(name="fn"),
          BinaryCategoricalFalsePositives(name="fp"),
          BinaryCategoricalTrueNegatives(name="tn"),
          BinaryCategoricalTruePositives(name="tp"),
      ])
  model.summary()

  train_config = get_train_config(name, balanced, stft_n_fft=stft_n_fft)

  try:
    model.fit(x=train_ds, validation_data=val_ds, **train_config)
  finally:
    model.save(os.path.join(get_ckpt_dir(name), LATEST_MODEL_NAME))


if __name__ == "__main__":
  fire.Fire(train)
