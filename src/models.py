"""Define keras model classes."""
# pylint: disable=arguments-differ
from enum import Enum

import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import (GRU, Activation, Add, AveragePooling2D,
                                     BatchNormalization, Conv1D, Conv2D, Dense,
                                     Dropout, GlobalAveragePooling2D, Input,
                                     Layer, LeakyReLU, MaxPool2D)

from src.data import get_norm_layer


class MFMLayer(str, Enum):
  """Layer types supported by an MFMBlock."""

  DENSE = "dense"
  CONV2D = "conv2d"


class MFMBlock(Layer):
  """Max Filter Map (MFM) block."""

  def __init__(self,
               out_features,
               kernel_initializer="glorot_normal",
               bias_initializer="zeros",
               layer_type=MFMLayer.CONV2D,
               **kwargs):
    super().__init__(name=kwargs.get("name"))
    self.out_features = out_features
    self.kernel_initializer = kernel_initializer
    self.bias_initializer = bias_initializer
    self.layer_type = layer_type
    if self.layer_type == MFMLayer.DENSE:
      layer_cls = Dense
    elif self.layer_type == MFMLayer.CONV2D:
      layer_cls = Conv2D
    else:
      raise NotImplementedError(f"MFMLayer with type {self.layer_type}")
    self.layer = layer_cls(
        self.out_features * 2,
        kernel_initializer=self.kernel_initializer,
        bias_initializer=self.bias_initializer,
        **kwargs)

  def get_config(self):  # noqa
    config = super().get_config()
    config.update({
        "out_features": self.out_features,
        "kernel_initializer": self.kernel_initializer,
        "bias_initializer": self.bias_initializer,
        "layer_type": self.layer_type
    })
    return config

  def call(self, inputs):
    """Split the output of the layer and return the maximum between them."""
    act = self.layer(inputs)
    out1, out2 = tf.split(act, num_or_size_splits=2, axis=-1)
    return tf.maximum(out1, out2)


def ResidualBlock(input_data,
                  filters,
                  conv_size,
                  strides,
                  first=False,
                  downsample=False):
  """Builds a resnet block """

  x = input_data
  if not first:
    x = LeakyReLU(alpha=0.01)(x)
    x = BatchNormalization(axis=2, epsilon=1e-05, momentum=0.1)(x)

  x = Conv2D(
      filters, conv_size, strides=strides, activation=None, padding='same')(
          x)

  x = BatchNormalization(epsilon=1e-05, momentum=0.1)(x)
  x = LeakyReLU(alpha=0.01)(x)

  x = Conv1D(
      filters,
      conv_size[0] * conv_size[1],
      strides=1,
      activation=None,
      padding='same')(
          x)

  if downsample:
    input_data = Conv2D(
        filters, conv_size, strides=strides, activation=None, padding='same')(
            input_data)

  x = Add()([x, input_data])

  return x


def BlockOfResidualBlocks(input_data, n_blocks, last_filters, filters,
                          conv_size, strides, downsample):

  for _ in range(n_blocks - 1):
    input_data = ResidualBlock(input_data, last_filters, conv_size, strides=1)

  if downsample:
    input_data = ResidualBlock(
        input_data,
        filters,
        conv_size,
        strides,
        first=False,
        downsample=downsample)

  return input_data


class MFMGroup(Layer):
  """Max Filter Map (MFM) block group."""

  def __init__(self,
               out_filters,
               kernel_size=3,
               strides=(1, 1),
               padding="same"):
    super().__init__()
    self.out_filters = out_filters
    self.kernel_size = kernel_size
    self.strides = strides
    self.padding = padding
    self.block1 = None
    self.block2 = None

  def build(self, input_shape):  # noqa
    self.block1 = MFMBlock(
        out_features=input_shape[-1],
        kernel_size=1,
        strides=(1, 1),
        padding="same",
        layer_type=MFMLayer.CONV2D)
    self.block2 = MFMBlock(
        out_features=self.out_filters,
        kernel_size=self.kernel_size,
        strides=self.strides,
        padding=self.padding,
        layer_type=MFMLayer.CONV2D)

  def get_config(self):  # noqa
    config = super().get_config()
    config.update({
        "out_filters": self.out_filters,
        "kernel_size": self.kernel_size,
        "strides": self.strides,
        "padding": self.padding
    })
    return config

  def call(self, inputs):  # noqa
    act = self.block1(inputs)
    out = self.block2(act)
    return out


def LightCNN(inputs):
  """Define LightCNN model from the e SJTU Robust Anti-spoofing System.

  Refer to: https://www.isca-speech.org/archive/Interspeech_2019/pdfs/2170.pdf
  """
  mfm_out = MFMBlock(
      out_features=16,
      kernel_size=5,
      padding="same",
      layer_type=MFMLayer.CONV2D)(
          inputs)
  for out_filters in [24, 32, 16, 16]:
    mfm_out = MaxPool2D(pool_size=(2, 2))(mfm_out)
    mfm_out = MFMGroup(out_filters=out_filters)(mfm_out)
  mfm_out = MaxPool2D(pool_size=(2, 2))(mfm_out)
  # We can't use keras Reshape layer since the axis 1 is dynamic, i.e. each
  # batch can have a different number of frames.
  mfm_flat = tf.reshape(
      mfm_out,
      shape=[
          tf.shape(mfm_out)[0],
          tf.shape(mfm_out)[1], mfm_out.shape[-2] * mfm_out.shape[-1]
      ])
  mfm_flat = tf.reduce_mean(mfm_flat, axis=1)
  fc_mfm = MFMBlock(
      out_features=64, layer_type=MFMLayer.DENSE, name="fc_mfm_block")(
          mfm_flat)
  fc_mfm = Dropout(rate=0.5)(fc_mfm)
  fc_mfm = Dense(units=64, kernel_initializer="glorot_normal")(fc_mfm)
  fc_mfm = Dropout(rate=0.5)(fc_mfm)
  pred = Dense(units=2, kernel_initializer="glorot_normal")(fc_mfm)
  lightcnn = Model(inputs=inputs, outputs=pred, name="lightcnn")
  return lightcnn


def ResNet(inputs):
  """Define Resnet model from Dynamically Mitigating Data Discrepancy with
    Balanced Focal Loss for Replay Attack Detection

  Refer to: https://arxiv.org/abs/2006.14563
  """

  x = Conv2D(16, (3, 7), strides=(1, 1), padding='same')(inputs)
  x = BatchNormalization(epsilon=1e-05, momentum=0.1)(x)
  x = LeakyReLU(alpha=0.01)(x)

  x = ResidualBlock(x, 32, (3, 7), strides=(1, 1), first=True, downsample=True)
  x = BlockOfResidualBlocks(x, 1, 32, 64, (3, 7), (2, 4), downsample=True)
  x = BlockOfResidualBlocks(x, 2, 64, 64, (3, 7), (2, 4), downsample=True)
  x = BlockOfResidualBlocks(x, 2, 64, 128, (3, 7), (2, 4), downsample=True)
  x = BlockOfResidualBlocks(x, 2, 128, 128, (3, 7), (2, 4), downsample=False)

  x = BatchNormalization(epsilon=1e-05, momentum=0.1)(x)
  x = LeakyReLU(alpha=0.01)(x)

  x = AveragePooling2D((1, 17), padding='same')(x)
  x = MaxPool2D((1, 17), padding='same')(x)

  x = tf.reshape(
      x, shape=[tf.shape(x)[0],
                tf.shape(x)[1], x.shape[-2] * x.shape[-1]])

  x = GRU(512)(x)
  x = Dense(64)(x)
  outputs = Dense(2)(x)

  resnet = Model(inputs=inputs, outputs=outputs, name="resnet")
  return resnet
