import enum
import math
import os
import pickle
import warnings

import librosa
import numpy as np
import soundfile as sf
import tensorflow as tf
from kaldi.feat.functions import sliding_window_cmn
from kaldi.matrix import Matrix
from tensorflow.keras.layers.experimental.preprocessing import Normalization
from tqdm import tqdm

from src.consts import (BATCH_SIZE, CMN_OPTS, CQT_FMIN, CQT_HOP_LENGTH,
                        CQT_NUM_OCTAVES, FILE_DS_BUFFER_SIZE, LABELS, SEED,
                        STFT_HOP_LENGTH_MS, STFT_N_FFT, STFT_WIN_LENGTH_MS)
from src.util import get_assets_dir, get_batch_size, get_samples


class FeatType(str, enum.Enum):
  """Feature pre-processing type."""

  MAG = "mag"
  PHASE = "phase"
  BOTH = "both"


def eager_map(out_dtypes=None):
  """Decorator to convert python function to tensorflow one.

  Args:
    out_dtypes: The list of output dtypes for the function. If None is
      specified, defaults to the same type as the input types.
   """

  def eager_map_decorator(func):

    def wrapper(*args):
      Tout = [arg.dtype for arg in args] if out_dtypes is None else out_dtypes
      return tf.py_function(func, inp=args, Tout=Tout)

    return wrapper

  return eager_map_decorator


@eager_map(out_dtypes=[tf.float32, tf.int32])
def audio_file_to_sample(audio_file,
                         label,
                         feat_type=FeatType.MAG,
                         cqt=False,
                         stft_n_fft=512):
  """Convert an audio file and the label to an input sample (x, y)."""
  data, samplerate = sf.read(audio_file.numpy(), dtype="float32")
  cur_win_length = samplerate * STFT_WIN_LENGTH_MS // 1000
  cur_hop_length = samplerate * STFT_HOP_LENGTH_MS // 1000
  if cqt:
    with warnings.catch_warnings():
      warnings.simplefilter("ignore")
      num_bins = int(stft_n_fft // 2 + 1)
      bins_per_octave = math.ceil(num_bins / CQT_NUM_OCTAVES)
      Z = librosa.cqt(
          data,
          samplerate,
          hop_length=cur_hop_length,
          fmin=CQT_FMIN,
          n_bins=num_bins,
          bins_per_octave=bins_per_octave)
  else:
    Z = librosa.core.stft(
        data,
        n_fft=stft_n_fft,
        win_length=cur_win_length,
        hop_length=cur_hop_length)
  feats = []
  if feat_type in [FeatType.PHASE, FeatType.BOTH]:
    feats.append(np.angle(Z).T)
  if feat_type in [FeatType.MAG, FeatType.BOTH]:
    S = np.abs(Z)
    S_db = np.log(S**2 + 1e-12)
    S_db = Matrix(S_db.T)
    S_out = Matrix(*S_db.shape)
    sliding_window_cmn(CMN_OPTS, S_db, S_out)
    feats.append(S_out.numpy())
  feats = np.stack(feats, axis=-1)
  return feats, label


@eager_map(out_dtypes=[tf.float32, tf.float32])
def tile_pad_batch(padded_x_batch, y_batch, len_batch):
  """Pad batches using tile. Assumes that the time axis comes first, and
  feature axis is last.
  """
  len_batch = len_batch.numpy()
  padded_x_batch = padded_x_batch.numpy()
  pad_frames = padded_x_batch.shape[1]
  tile_padded_x_batch = np.empty(padded_x_batch.shape)
  for i, (padded_x, l) in enumerate(zip(padded_x_batch, len_batch)):
    unpadded_x = padded_x[:l, ...]
    tile_padded_x = np.pad(
        unpadded_x,
        pad_width=((0, pad_frames - l), (0, 0), (0, 0)),
        mode='wrap')
    tile_padded_x_batch[i] = tile_padded_x

  # One-hot encode labels for categorical cross-entropy
  onehot_y_batch = tf.one_hot(y_batch, len(LABELS))
  return tile_padded_x_batch, tf.cast(onehot_y_batch, tf.float32)


def get_dataset_for_label(splits,
                          label,
                          feat_type=FeatType.MAG,
                          cqt=False,
                          feature_norm=False,
                          stft_n_fft=512,
                          access_type="LA",
                          shuffle=True):
  """Get a Dataset object for the given label.

  Args:
    split: List of strings with dataset splits. One of {'train', 'dev', 'eval'}.
    label: String with class label. One of {'bonafide', 'spoofed'}
    cqt: Whether to include the CQT spectrogram with the input features.
    feature_norm: Whether to apply feature-wise normalization to inputs.
    stft_n_fft: The number of bins to use when extracting the STFT of the raw
      input signal.
    access_type: String with the access type of the spoofed data.

  Returns:
    A Dataset object that yields an (image, label) sample, where image is a
    float32 tensor with 3 channels and label is an integer that is 0 for benign
    and 1 for malignant.

  """
  assert label in LABELS, f"Unrecognized label {label}."
  fnames = []
  for split in splits:
    fnames.extend(get_samples(split, label, access_type, shuffle=shuffle))
  files_ds = tf.data.Dataset.from_tensor_slices(fnames)
  if shuffle:
    files_ds = files_ds.shuffle(FILE_DS_BUFFER_SIZE, seed=SEED)
  labeled_ds = files_ds.map(
      lambda f: audio_file_to_sample(f, tf.constant(LABELS.index(label)),
                                     feat_type, cqt, stft_n_fft),
      num_parallel_calls=12)
  if feature_norm:
    norm_layer = get_norm_layer(
        splits=["train"],
        feat_type=feat_type,
        cqt=cqt,
        stft_n_fft=stft_n_fft,
        access_type=access_type,
        shuffle=shuffle)
    labeled_ds = labeled_ds.map(lambda x, y: (norm_layer(x), y))
  return labeled_ds


def adapted_norm_layer(splits,
                       feat_type=FeatType.MAG,
                       cqt=False,
                       stft_n_fft=512,
                       access_type="LA",
                       shuffle=True):
  get_x = lambda x, _: x
  pos_xs_ds = get_dataset_for_label(
      splits,
      LABELS[0],
      feat_type=feat_type,
      cqt=cqt,
      stft_n_fft=stft_n_fft,
      feature_norm=False,
      access_type=access_type,
      shuffle=shuffle).map(get_x)
  neg_xs_ds = get_dataset_for_label(
      splits,
      LABELS[1],
      feat_type=feat_type,
      cqt=cqt,
      stft_n_fft=stft_n_fft,
      feature_norm=False,
      access_type=access_type,
      shuffle=shuffle).map(get_x)
  split_ds = pos_xs_ds.concatenate(neg_xs_ds).prefetch(BATCH_SIZE * 5)
  norm_layer = Normalization(axis=(-2, -1))
  for x in tqdm(split_ds, desc="Computing feature mean and std"):
    norm_layer.adapt(x)
  return norm_layer


def get_norm_layer(splits,
                   feat_type=FeatType.MAG,
                   cqt=False,
                   stft_n_fft=512,
                   access_type="LA",
                   shuffle=True):
  """Retrieve a normalization layer fitted to the given splits.

  The last axis of the data should be the feature axis that we wish to
  normalize.
  """
  feat_type_value = FeatType(feat_type).value
  feat_type_str = f"-{feat_type_value}"
  cqt_str = "-cqt" if cqt else "-stft"
  n_fft_str = f"-n_fft={stft_n_fft}"

  fname = get_assets_dir(
      "-".join(splits) +
      f"{cqt_str}{n_fft_str}{feat_type_str}-{access_type}.pkl")

  if os.path.exists(fname):
    with open(fname, "rb") as fhandle:
      norm_layer = pickle.load(fhandle)
      return norm_layer

  norm_layer = adapted_norm_layer(
      splits,
      feat_type=feat_type,
      cqt=cqt,
      stft_n_fft=stft_n_fft,
      access_type=access_type,
      shuffle=shuffle)
  with open(fname, "wb") as fhandle:
    pickle.dump(norm_layer, fhandle, protocol=pickle.HIGHEST_PROTOCOL)
  return norm_layer


def split_dataset(dataset, ratio):
  """Splits dataset into two separate ones using ratio."""
  split_percent = round(ratio * 100)

  dataset = dataset.enumerate()
  val_ds = dataset.filter(lambda f, data: f % 100 < split_percent)
  train_ds = dataset.filter(lambda f, data: f % 100 >= split_percent)

  # Remove enumeration
  val_ds = val_ds.map(lambda f, data: data)
  train_ds = train_ds.map(lambda f, data: data)

  return train_ds, val_ds


def get_padded_dataset(pos_ds, neg_ds, balanced, repeat, stft_n_fft=512):
  if balanced:
    pos_ds = pos_ds.repeat(repeat)
    neg_ds = neg_ds.repeat(repeat)
    balanced_ds = tf.data.experimental.sample_from_datasets(
        datasets=[pos_ds, neg_ds], weights=[0.5, 0.5], seed=SEED)
    split_ds = balanced_ds
  else:
    split_ds = pos_ds.concatenate(neg_ds).repeat(repeat)

  dataset_with_len = split_ds.map(lambda x, y: (x, y, tf.shape(x)[0]))
  batch_size = get_batch_size(stft_n_fft)
  padded_ds = dataset_with_len.padded_batch(
      batch_size, padded_shapes=([None, None, None], [], []))
  padded_ds = padded_ds.map(tile_pad_batch).prefetch(5)
  return padded_ds


def get_dataset(splits,
                feature_norm=False,
                feat_type=FeatType.MAG,
                cqt=False,
                balanced=False,
                stft_n_fft=512,
                access_type="LA",
                shuffle=True,
                repeat=None):
  """Retrieve a dataset with or without augmentation.

  Args:
    splits: A list of splits to retrieve the dataset from. Can contain any of
      {'train', 'dev', 'eval'}.
    feature_norm: Whether to apply a feature-wise normalization to the dataset.
    cqt: Whether to use CQT spectrogram features with inputs.
    stft_n_fft: The number of bins to use when extracting the STFT of the raw
      input signal.
    balance: Whether to balance the false and positive classes in each batch
      of data.
    repeat: How many times to repeat each dataset. If None repeats indefinitely.

  Returns:
    A tensorflow Dataset object that iterates over sample batches.
  """
  assert len(set(splits)) == len(splits), "Only unique splits can be specified."

  # Retrieve positive and negative samples for all splits
  pos_ds = get_dataset_for_label(
      splits,
      LABELS[0],
      feat_type=feat_type,
      cqt=cqt,
      stft_n_fft=stft_n_fft,
      feature_norm=feature_norm,
      access_type=access_type,
      shuffle=shuffle,
  )
  neg_ds = get_dataset_for_label(
      splits,
      LABELS[1],
      feat_type=feat_type,
      cqt=cqt,
      stft_n_fft=stft_n_fft,
      feature_norm=feature_norm,
      access_type=access_type,
      shuffle=shuffle,
  )
  return get_padded_dataset(
      pos_ds, neg_ds, balanced=balanced, repeat=repeat, stft_n_fft=stft_n_fft)
