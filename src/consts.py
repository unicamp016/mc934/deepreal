"""Project constants."""
from kaldi.feat.functions import SlidingWindowCmnOptions

SEED = 123
DATASET_DIR = "data"
SPLITS = ["train", "dev", "eval"]
LABELS = ["bonafide", "spoof"]
ACCESS_TYPES = ["LA", "PA"]
FILE_DS_BUFFER_SIZE = 2048  # Used to shuffle filenames
FEATURE_DS_BUFFER_SIZE = 258  # Used to shuffle images
BATCH_SIZE = 8
EPOCHS = 200
TRAIN_NEG_SIZE = 2580
TRAIN_POS_SIZE = 22800
DEV_NEG_SIZE = 2548
DEV_POS_SIZE = 22296
EVAL_NEG_SIZE = 7355
EVAL_POS_SIZE = 63882
TOTAL_POS = TRAIN_POS_SIZE + DEV_POS_SIZE
TOTAL_NEG = TRAIN_NEG_SIZE + DEV_NEG_SIZE

# STFT params
STFT_N_FFT = 512
# STFT_N_FFT = 1024
STFT_WIN_LENGTH_MS = 25
STFT_HOP_LENGTH_MS = 8

# CMVN params
CMN_CENTER = True
CMN_NORM_VAR = True
CMN_WINDOW = 300
CMN_OPTS = SlidingWindowCmnOptions()
CMN_OPTS.center = CMN_CENTER
CMN_OPTS.cmn_window = CMN_WINDOW
CMN_OPTS.normalize_variance = CMN_NORM_VAR
CMN_OPTS.max_warnings = 0

# CQT params
FS = 16000
CQT_NUM_OCTAVES = 8
CQT_HOP_LENGTH = 128
CQT_FMAX = FS / 2  # Nyquist frequency
CQT_FMIN = CQT_FMAX / (2**CQT_NUM_OCTAVES)

# train
MODELS = [ 'lightcnn', 'resnet' ]
