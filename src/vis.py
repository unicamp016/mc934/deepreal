"""Sound visualization functions."""
import contextlib
import os.path
import sys
from collections import defaultdict

import librosa
import librosa.display as libdisp
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import sklearn as sk
import sklearn.metrics
import sounddevice as sd
from tqdm import tqdm

import src.metrics as em
from src.consts import ACCESS_TYPES, LABELS, SPLITS
from src.util import get_score_filename, get_tensorboard_scalars

matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem":
        "pdflatex",
    "font.family":
        "serif",
    "text.usetex":
        True,
    "pgf.rcfonts":
        False,
    "pgf.preamble": [
        r"\usepackage[utf8x]{inputenc}",
        r"\usepackage[T1]{fontenc}",
        r"\usepackage{amsmath}",
    ]
})


@contextlib.contextmanager
def plt_fig(save, *args, **kwargs):
  """Context manager to handle matplotlib figures."""
  fig, axes = plt.subplots(*args, **kwargs)
  try:
    yield fig, axes
    if save is None:
      fig.waitforbuttonpress()
    else:
      fig.savefig(save)
  finally:
    plt.close(fig)


def plot_sound(data, samplerate, save=None):
  """Plot the sound amplitude over time.

  Args:
    data: A 1-D array with the raw sound data.
    samplerate: The sampling rate used to collect the sound data.
  """
  with plt_fig(save) as (_, ax):
    num_samples = len(data)
    t = np.arange(num_samples) / samplerate
    ax.plot(t, data)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Amplitude")


def plot_spectrogram(S, save=None):
  """Plot the spectrogram of the audio in the frequency domain.

  Args:
    Z: A 2-D real matrix with the magnitude output of the STFT. The first axis
      should be the frequency bins and the second axis represent time.
  """
  with plt_fig(save) as (fig, ax):
    img = libdisp.specshow(
        librosa.amplitude_to_db(S, ref=np.max),
        y_axis='log',
        x_axis='time',
        ax=ax)
    ax.set_title('Power spectrogram')
    fig.colorbar(img, ax=ax, format="%+2.0f dB")


def plot_cmvn(S, save=None):
  """Plot the audio spectrogram after applying CMVN normalization."""
  with plt_fig(save) as (_, ax):
    ax.imshow(S.T)
    ax.set_title("CMVN Spectrogram")
    ax.set_ylabel("Frequency (Hz)")
    ax.set_xlabel("Time (s)")


def play_sound(data, samplerate):
  """Plays the sound data using PulseAudio.

  Args:
    data: A 1-D array with the raw sound data.
    samplerate: The sampling rate used to collect the sound data.
  """
  sd.play(data, samplerate)
  return sd.wait()


def vis_batch(dataset, axes_shape=(3, 3)):
  plt.figure(figsize=(10, 10))
  images_per_show = axes_shape[0] * axes_shape[1]
  for images, labels in dataset.take(1):
    images = np.squeeze(images)
    labels = np.argmax(labels, axis=1)
    batch_size = len(images)
    print(f"Batch size: {batch_size}")
    for label_id, class_name in enumerate(LABELS):
      nr_labels = np.count_nonzero(labels == label_id)
      label_perc = nr_labels * 100 / batch_size
      print(
          f"{class_name} ({label_id}) labels: {nr_labels} ({label_perc:.2f}%)")
    nr_shows = batch_size // images_per_show
    nr_shows += bool(batch_size % images_per_show)
    for b in range(nr_shows):
      for ax_id, img_id in enumerate(
          range(b * images_per_show, min((b + 1) * images_per_show,
                                         len(images)))):
        ax = plt.subplot(axes_shape[0], axes_shape[1], ax_id + 1)
        img = images[img_id]
        ax.imshow(img)
        ax.set_title(LABELS[labels[img_id]])
        plt.axis("off")
      press = plt.waitforbuttonpress()
      plt.close("all")
      if not press:
        break


def vis_epoch(dataset, steps_per_epoch):
  total_nr_labels = defaultdict(lambda: 0)
  with tqdm(total=steps_per_epoch) as pbar:
    for _, y in dataset.take(steps_per_epoch):
      labels = np.argmax(y, axis=1)
      for label_id, class_name in enumerate(LABELS):
        total_nr_labels[class_name] += np.count_nonzero(labels == label_id)
      pbar.update(1)

  total_samples = sum(total_nr_labels.values())
  for class_name, label_count in total_nr_labels.items():
    label_perc = label_count * 100 / total_samples
    print(f"{class_name} ({LABELS.index(class_name)}) "
          f"labels: {label_count} ({label_perc:.2f}%)")


def plot_cm_scores(names, bona_cms, spoof_cms, save=None):
  with plt_fig(save, figsize=(5, 5)) as (_, ax):
    hist_kwargs = {
        "ax": ax,
        "hist": False,
        "kde": True,
        "kde_kws": {
            "shade": True,
            "linewidth": 1,
            "alpha": 0.4,
        },
    }
    for name, bona_cm, spoof_cm in zip(names, bona_cms, spoof_cms):
      sns.distplot(bona_cm, label=f'Boa fé ({name})', **hist_kwargs)
      sns.distplot(spoof_cm, label=f'Fraude ({name})', **hist_kwargs)
    ax.set(xlabel='Pontuação CM', ylabel='Densidade')
    ax.legend()


def plot_asv_scores(tar_asv, non_asv, spoof_asv, asv_threshold, save=None):
  with plt_fig(save, figsize=(5, 5)) as (_, ax):
    hist_kwargs = {
        "ax": ax,
        "hist": False,
        "kde": True,
        "kde_kws": {
            "shade": True,
            "linewidth": 1,
            "alpha": 0.5,
        }
    }
    sns.distplot(tar_asv, label="Alvo", **hist_kwargs)
    sns.distplot(non_asv, label="Não-alvo", **hist_kwargs)
    sns.distplot(spoof_asv, label="Fraude", **hist_kwargs)
    ax.plot(
        asv_threshold,
        0,
        'o',
        markersize=5,
        mfc='none',
        mew=2,
        clip_on=False,
        label='Limiar EER')
    ax.legend()
    ax.set_ylim(ymin=0)
    ax.set(xlabel='Pontuação ASV', ylabel='Densidade')


def plot_scores(names, tar_asv, non_asv, spoof_asv, asv_threshold, bona_cm_list,
                spoof_cm_list, save):
  save_asv, save_cm = None, None
  if save is not None:
    name, ext = os.path.splitext(save)
    save_asv = f"{name}-asv{ext}"
    save_cm = f"{name}-cm{ext}"
  plot_asv_scores(tar_asv, non_asv, spoof_asv, asv_threshold, save_asv)
  plot_cm_scores(names, bona_cm_list, spoof_cm_list, save_cm)


def plot_tdcf(names,
              tDCF_curve_list,
              CM_thresholds_list,
              save=None,
              verbose=False,
              file=sys.stdout):
  # Plot t-DCF as function of the CM threshold.
  with plt_fig(save, figsize=(6, 6)) as (_, ax):
    min_cm_thresh, max_cm_thresh = float("inf"), -float("inf")
    for name, tDCF_curve, CM_thresholds in zip(names, tDCF_curve_list,
                                               CM_thresholds_list):
      # Minimum t-DCF
      min_tDCF_index = np.argmin(tDCF_curve)
      min_tDCF = tDCF_curve[min_tDCF_index]

      if verbose:
        print('\nTANDEM', file=file)
        print('   min-tDCF       = {:8.5f}'.format(min_tDCF), file=file)

      ax.plot(CM_thresholds, tDCF_curve, label=f'$\\text{{t-DCF}}(s)$ [{name}]')
      ax.plot(
          CM_thresholds[min_tDCF_index],
          min_tDCF,
          'o',
          markersize=3,
          mfc='none',
          mew=2,
          label=f'$\\text{{t-DCF}}_\\text{{min}} = {min_tDCF:.5f}$ [{name}]')
      min_cm_thresh = min(min_cm_thresh, np.min(CM_thresholds))
      max_cm_thresh = max(max_cm_thresh, np.max(CM_thresholds))

    ax.set(
        xlabel='Limiar CM',
        ylabel='$\\text{t-DCF}_\\text{norm}$',
        xlim=[min_cm_thresh, max_cm_thresh],
        ylim=[0, 1.5])
    ax.plot([min_cm_thresh, max_cm_thresh], [1, 1],
            '--',
            color='black',
            label='CM arbitrariamente ruim ($\\text{t-DCF}_\\text{norm} = 1$)')
    ax.legend()


def plot_scores_and_tdcf(names,
                         cm_labels_list,
                         cm_scores_list,
                         split,
                         access_type,
                         save_scores=None,
                         save_tdcf=None,
                         verbose=True,
                         file=sys.stdout):
  """Plot the asv and cm histograms.

  Args:
    cm_data: cm_data composed of keys and score
    split: split of the data to load the appropriately asv score
    access_type: access type of the data to load the appropriately asv score
  """
  asv_score_file = get_score_filename(split, access_type)

  # Fix tandem detection cost function (t-DCF) parameters
  Pspoof = 0.05
  cost_model = {
      'Pspoof': Pspoof,  # Prior probability of a spoofing attack
      'Ptar': (1 - Pspoof) * 0.99,  # Prior probability of target speaker
      'Pnon': (1 - Pspoof) * 0.01,  # Prior probability of nontarget speaker
      'Cmiss_asv': 1,  # Cost of ASV system falsely rejecting target speaker
      'Cfa_asv': 10,  # Cost of ASV system falsely accepting nontarget speaker
      'Cmiss_cm': 1,  # Cost of CM system falsely rejecting target speaker
      'Cfa_cm': 10,  # Cost of CM system falsely accepting spoof
  }

  # Load organizers' ASV scores
  asv_data = np.genfromtxt(asv_score_file, dtype=str)
  asv_keys = asv_data[:, 1]
  asv_scores = asv_data[:, 2].astype(np.float)

  # Extract target, nontarget, and spoof scores from the ASV scores
  tar_asv = asv_scores[asv_keys == 'target']
  non_asv = asv_scores[asv_keys == 'nontarget']
  spoof_asv = asv_scores[asv_keys == 'spoof']

  # Extract bona fide (real human) and spoof scores from the CM scores
  bona_cm_list, spoof_cm_list, eer_cm_list = [], [], []
  for cm_scores, cm_labels in zip(cm_scores_list, cm_labels_list):
    bona_cm = cm_scores[cm_labels == LABELS.index('bonafide')]
    spoof_cm = cm_scores[cm_labels == LABELS.index('spoof')]
    eer_cm_list.append(em.compute_eer(bona_cm, spoof_cm)[0])
    bona_cm_list.append(bona_cm)
    spoof_cm_list.append(spoof_cm)

  # EERs of the standalone systems and fix ASV operating point to EER threshold
  eer_asv, asv_threshold = em.compute_eer(tar_asv, non_asv)

  Pfa_asv, Pmiss_asv, Pmiss_spoof_asv = em.obtain_asv_error_rates(
      tar_asv, non_asv, spoof_asv, asv_threshold)

  # Compute t-DCF
  tDCF_curve_list, CM_thresholds_list = em.compute_tDCF(
      bona_cm_list,
      spoof_cm_list,
      Pfa_asv,
      Pmiss_asv,
      Pmiss_spoof_asv,
      cost_model,
      print_cost=verbose,
      file=file,
  )

  if verbose:
    print('ASV SYSTEM', file=file)

    print(
        f'   EER            = {eer_asv * 100:8.5f} % '
        '(Equal error rate (target vs. nontarget discrimination)',
        file=file)

    print(
        f'   Pfa            = {Pfa_asv * 100:8.5f} % '
        '(False acceptance rate of nontargets)',
        file=file)

    print(
        f'   Pmiss          = {Pmiss_asv * 100:8.5f} % '
        '(False rejection rate of targets)',
        file=file)

    print(
        f'   1-Pmiss,spoof  = {(1 - Pmiss_spoof_asv) * 100:8.5f} % '
        '(Spoof false acceptance rate)',
        file=file)

    print('\nCM SYSTEM', file=file)
    for name, eer_cm in zip(names, eer_cm_list):
      print(f'   Experiment {name}:', file=file)
      print(
          f'   EER            = {eer_cm * 100:8.5f} % '
          '(Equal error rate for countermeasure)',
          file=file)

  plot_scores(names, tar_asv, non_asv, spoof_asv, asv_threshold, bona_cm_list,
              spoof_cm_list, save_scores)

  plot_tdcf(names, tDCF_curve_list, CM_thresholds_list, save_tdcf, verbose,
            file)


def plot_confusion_matrix(labels, preds, save=None):
  """Plot the confusion matrix

  Args:
    labels: ground truth (correct) target values.
    preds: estimated targets as returned by a classifier.
  """
  tn, fp, fn, tp = sk.metrics.confusion_matrix(labels, preds).ravel()
  cm = np.array([[tn, fp], [fn, tp]]).astype('int32')

  with plt_fig(save) as (fig, ax):
    # plt.figure(figsize=(5, 5), dpi=100)

    sns.heatmap(
        cm, ax=ax, annot=True, annot_kws={"fontsize": "xx-large"}, fmt="d")
    ax.set(ylabel='Rótulo', xlabel='Predição')


def to_label(s):
  return s.replace('_', ' ').capitalize()


def plot_roc_curves(experiments, labels_list, preds_list, save=None):
  """Plot the roc curves from the experiment label, and prediction lists

  Args:
    experiments: list of experiment names
    labels_list: list of labels for each experiment
    preds_list: list of predictions for each experiment
  """

  with plt_fig(save) as (_, ax):
    for experiment, labels, preds in zip(experiments, labels_list, preds_list):
      fpr, tpr, _ = sk.metrics.roc_curve(labels, preds)
      auc = sk.metrics.auc(fpr, tpr)
      ax.plot(
          100 * fpr,
          100 * tpr,
          ':',
          label=to_label(f'{experiment}, AUC = {auc}'),
          linewidth=1)
      ax.set(xlabel='False positives [%]', ylabel='True positives [%]')
      ax.legend()


def plot_tensorboard_metrics(experiments, splits, step, metrics, save=None):
  df = dict()

  # load dataframes
  for experiment in experiments:
    for split in splits:

      # assumes that there is only one tfevents valid file
      d = get_tensorboard_scalars(experiment, split, step)[0]

      if experiment in df.keys():
        df[experiment][split] = d
      else:
        df[experiment] = {split: d}

  for metric in metrics:

    s = save
    if save is not None:
      f, ext = os.path.splitext(save)
      s = '-'.join([f, metric + ext])

    with plt_fig(s, figsize=(6, 6)) as (_, ax):
      max_len = 0
      for experiment in experiments:
        for split in splits:
          steps = df[experiment][split][step]
          ax.plot(
              steps,
              df[experiment][split][metric],
              '--',
              label=' '.join([experiment, split]))
        max_len = max(max_len, len(steps))
      ax.set_yscale("log")
      ax.set_ylabel("$\mathcal{L}$")
      ax.set_xlabel("Época")
      ax.set_xticks(np.arange(0, max_len, 2))
      ax.legend()
      plt.tight_layout()
