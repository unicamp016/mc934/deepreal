"""Utility functions."""
import functools
import math
import os

import pandas as pd
import tensorflow.keras as keras
import tensorflow.keras.callbacks as cb
from tensorboard.backend.event_processing.event_accumulator import \
    EventAccumulator

from src.consts import (ACCESS_TYPES, DATASET_DIR, DEV_NEG_SIZE, DEV_POS_SIZE,
                        EPOCHS, LABELS, SPLITS, TRAIN_NEG_SIZE, TRAIN_POS_SIZE)
from src.metrics import (BinaryCategoricalFalseNegatives,
                         BinaryCategoricalFalsePositives,
                         BinaryCategoricalTrueNegatives,
                         BinaryCategoricalTruePositives)

BEST_MODEL_NAME = "best_model"
LATEST_MODEL_NAME = "latest_model"
DATASET_NAME = "ASVspoof2019"


def get_dataset_dir(split, access_type="LA"):
  """Get the base directory with input filenames for the split."""
  assert split in SPLITS, "Invalid split name."
  return os.path.join(DATASET_DIR, access_type,
                      f"{DATASET_NAME}_{access_type}_{split}", "flac")


def get_score_filename(split, access_type="LA"):
  """Return the filename with the asv scores for the given split."""
  assert split in SPLITS
  assert access_type in ACCESS_TYPES
  return os.path.join(
      DATASET_DIR, access_type, f"{DATASET_NAME}_{access_type}_asv_scores",
      f"{DATASET_NAME}.{access_type}.asv.{split}.gi.trl.scores.txt")


@functools.lru_cache(maxsize=128)
def get_labels(split, access_type="LA"):
  """Return a map from label to set, with each set containing the filenames for
  the label.
  """
  assert split in SPLITS, "Invalid split name."
  assert access_type in ACCESS_TYPES, "Invalid access type."
  ext = "trn" if split == "train" else "trl"
  split_labels_fname = os.path.join(
      DATASET_DIR, access_type, f"{DATASET_NAME}_{access_type}_cm_protocols",
      f"{DATASET_NAME}.{access_type}.cm.{split}.{ext}.txt")
  label_df = pd.read_csv(
      split_labels_fname,
      sep=" ",
      names=["speaker", "filename", "physical", "system", "label"])
  label_dict = {}
  for label in LABELS:
    fnames = label_df[label_df["label"] == label]["filename"]
    label_dict[label] = fnames
  return label_dict


@functools.lru_cache(maxsize=128)
def get_samples(split, label, access_type="LA", shuffle=True):
  """Return the list of filenames for the given split and label."""
  assert label in LABELS, "Invalid class label name."
  label_dict = get_labels(split, access_type)
  base_fnames = label_dict[label]
  if not shuffle:
    base_fnames.sort_values()
  return [
      os.path.join(get_dataset_dir(split, access_type), f"{bfname}.flac")
      for bfname in base_fnames
  ]


def load_model(model_path):
  return keras.models.load_model(
      model_path,
      custom_objects={
          "BinaryCategoricalTrueNegatives": BinaryCategoricalTrueNegatives,
          "BinaryCategoricalTruePositives": BinaryCategoricalTruePositives,
          "BinaryCategoricalFalsePositives": BinaryCategoricalFalsePositives,
          "BinaryCategoricalFalseNegatives": BinaryCategoricalFalseNegatives,
      })


def get_best_model(model_name):
  """Retrieve the best performing model for `model_name` experiment."""
  model_path = os.path.join(get_ckpt_dir(model_name), BEST_MODEL_NAME)
  return load_model(model_path)


def get_latest_model(model_name):
  """Retrieve the latest trained model for `model_name` experiment."""
  model_path = os.path.join(get_ckpt_dir(model_name), LATEST_MODEL_NAME)
  return load_model(model_path)


def get_assets_dir(path):
  """Directory to project assets."""
  return os.path.join("assets", path)


def get_log_dir(path):
  """Directory to log training info."""
  return os.path.join("logs", path)


def get_ckpt_dir(path):
  """Directory to store model checkpoints."""
  return os.path.join("ckpts", path)


def get_batch_size(stft_n_fft):
  """Return the batch size depending on the size of the inputs."""
  return int(2**(12 - math.log(stft_n_fft, 2)))


def get_train_config(name, balanced, stft_n_fft=512):
  """Return a dictionary with the training configuration parameters."""
  # Specify steps per epoch for the balanced dataset
  batch_size = get_batch_size(stft_n_fft)
  if balanced:
    steps_per_epoch = math.ceil(2 * max(TRAIN_POS_SIZE, TRAIN_NEG_SIZE) /
                                batch_size)
  else:
    steps_per_epoch = math.ceil((TRAIN_POS_SIZE + TRAIN_NEG_SIZE) / batch_size)
  validation_steps = math.ceil((DEV_POS_SIZE + DEV_NEG_SIZE) / batch_size)

  # Define callbacks
  best_ckpt_callback = cb.ModelCheckpoint(
      os.path.join(get_ckpt_dir(name), BEST_MODEL_NAME),
      monitor="val_loss",
      mode="min",
      verbose=1,
      save_best_only=True,
      save_weights_only=False)
  last_ckpt_callback = cb.ModelCheckpoint(
      os.path.join(get_ckpt_dir(name), LATEST_MODEL_NAME),
      verbose=1,
      save_best_only=False,
      save_weights_only=False)
  reduce_lr_callback = cb.ReduceLROnPlateau(
      monitor="val_loss",
      mode="min",
      factor=0.1,
      patience=4,
      cooldown=1,
      verbose=True,
      min_delta=0.001)
  early_stopping_callback = cb.EarlyStopping(
      monitor="val_loss", mode="min", patience=8, verbose=1)
  tensorboard_callback = cb.TensorBoard(
      log_dir=get_log_dir(name), histogram_freq=1, update_freq=1000)
  callbacks = [
      tensorboard_callback,
      reduce_lr_callback,
      early_stopping_callback,
      best_ckpt_callback,
      last_ckpt_callback,
  ]

  return {
      "epochs": EPOCHS,
      "validation_steps": validation_steps,
      "steps_per_epoch": steps_per_epoch,
      "callbacks": callbacks,
  }


def get_tfevents_dir(name, split):
  assert split in SPLITS
  if split == 'dev':
    split = 'validation'
  return os.path.join(get_log_dir(name), split)


def _tfevents_to_dataframe(tfevents_dir, step):
  """Load the tfevents file creating a `EventAccumulator` to load
  the scalars from tensorboard logs

  Args:
    tfevents_dir: the folder where the tfevents files are
    step: the step type (could be epoch and batch for instance)
  Returns:
    list: return a list of dataframes, each one of them representing one
    tfevents file
  """
  dfs = []

  for dname in os.listdir(tfevents_dir):
    ea = EventAccumulator(os.path.join(tfevents_dir, dname)).Reload()
    tags = list(t for t in ea.Tags()['scalars'] if t.startswith(step))
    column_names = dict((t, t.replace(step + '_', '')) for t in tags)
    rows = dict()

    for tag in tags:
      for event in ea.Scalars(tag):

        if event.step not in rows.keys():
          rows[event.step] = {step: event.step}
        rows[event.step][column_names[tag]] = event.value

    if rows:
      dfs.append(pd.DataFrame(data=rows.values()))
  return dfs


def get_tensorboard_scalars(name, split, step):
  return _tfevents_to_dataframe(get_tfevents_dir(name, split), step)
