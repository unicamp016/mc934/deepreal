"""Evaluate model."""
import json
import os

import fire
import numpy as np
import tensorflow as tf
from scipy.special import softmax
from tqdm import tqdm

from src.consts import ACCESS_TYPES, LABELS
from src.data import get_dataset
from src.util import get_best_model, get_log_dir
from src.vis import (plot_confusion_matrix, plot_roc_curves,
                     plot_scores_and_tdcf, plot_tensorboard_metrics)

RESULTS_DIR = "results"


def log_fname(name, fname=""):
  return os.path.join(get_log_dir(name), RESULTS_DIR, fname)


def results_fname(name, fname=""):
  return os.path.join(RESULTS_DIR, name, fname)


def pred_to_score(preds):
  return preds[:, LABELS.index("bonafide")]


def argmax(array):
  """Convert an (n, m) array to an (n, 1) array with the index of max values."""
  return np.argmax(array, axis=1)


def pred_to_prob(preds):
  """Convert model preds from two nodes [ x, y ] to a prob."""
  return softmax(preds, axis=1)[:, LABELS.index("spoof")]


def lazily_compute_eval_lists(names, split):
  """computes (if needed) the evaluation and scores of models"""
  labels_list, preds_list, probs_list, scores_list = [], [], [], []
  for name in names:
    # Define filenames
    log_dir = get_log_dir(name)
    preds_fname = log_fname(name, f"{split}-preds.npy")
    labels_fname = log_fname(name, f"{split}-labels.npy")

    # Load scores if they've already been computed.
    if os.path.isdir(log_dir) and os.path.isfile(preds_fname):
      preds = np.load(preds_fname)
      labels = np.load(labels_fname)
      labels_list.append(labels)
      preds_list.append(argmax(preds))
      probs_list.append(pred_to_prob(preds))
      scores_list.append(pred_to_score(preds))
      print(f"Loaded scores and predictions for {name}")
      continue

    os.makedirs(log_fname(name), exist_ok=True)

    # Load config to extract dataset params
    with open(os.path.join(log_dir, "config.json")) as cf:
      dataset_config = json.load(cf)
      dataset_config.pop("name")
      dataset_config.pop("balanced")
      dataset_config.pop("model")

    model = get_best_model(name)
    preds, labels = [], []
    split_ds = get_dataset(
        splits=[split], shuffle=False, repeat=1, **dataset_config)
    for x, y in tqdm(
        split_ds, desc=f"Predictions and labels for split {split} of {name}"):
      preds.append(model.predict(x))
      labels.append(argmax(y))

    preds = np.concatenate(preds, axis=0)
    labels = np.concatenate(labels, axis=0)
    preds_list.append(argmax(preds))
    probs_list.append(pred_to_prob(preds))
    labels_list.append(labels)
    scores_list.append(pred_to_score(preds))

    # Save predictions and labels to avoid recomputing afterwards.
    np.save(preds_fname, preds)
    np.save(labels_fname, labels)

  return labels_list, preds_list, probs_list, scores_list


def evaluate(*names, split="eval", access_type="LA"):
  assert access_type in ACCESS_TYPES, f"Invalid access type {access_type}"
  assert split in ["dev", "eval"], f"Invalid split name {split}"
  assert len(names) > 0

  (labels_list, preds_list, probs_list,
   scores_list) = lazily_compute_eval_lists(names, split)

  # Get the average score for each audio
  scores = np.mean(scores_list, axis=0)
  assert np.all(np.array(labels_list) == labels_list[0]), (
      "Make sure that evaluation samples were drawn in the same order in order "
      "to average their scores.")

  all_experiments_name = "-".join(names)
  results_dir = results_fname(all_experiments_name)
  os.makedirs(results_dir, exist_ok=True)

  for experiment, labels, preds in zip(names, labels_list, preds_list):
    print("Plotting Confusion Matrix...")
    plot_confusion_matrix(
        labels,
        preds,
        save=os.path.join(results_dir,
                          f"{experiment}-{split}-confusion-matrix.pdf"))
  print("Plotting the scores and tdcf graphs...")
  with open(os.path.join(results_dir, f"{split}.txt"), "w") as f:
    plot_scores_and_tdcf(
        names,
        labels_list,
        scores_list,
        split,
        access_type,
        save_scores=os.path.join(results_dir, f"{split}-scores.pdf"),
        save_tdcf=os.path.join(results_dir, f"{split}-tdcf.pdf"),
        file=f,
    )
  print("Plotting ROCs...")
  plot_roc_curves(
      names,
      labels_list,
      probs_list,
      save=os.path.join(results_dir, f"{split}-roc-curves.pdf"))

  print(f'Plotting tensorboard metrics...')
  plot_tensorboard_metrics(
      names,
      [ 'train', 'dev' ],
      'epoch',
      metrics = [ 'loss' ],
      save=os.path.join(results_dir, "metrics.pdf"))
  plot_tensorboard_metrics(
      # remove phase experiment from accuracy
      [ x for x in names if 'phase' not in x ], 
      [ 'dev' ],
      'epoch',
      metrics = [ 'accuracy' ],
      save=os.path.join(results_dir, "metrics.pdf"))
  print("Finished.")


if __name__ == "__main__":
  gpus = tf.config.experimental.list_physical_devices('GPU')
  tf.config.experimental.set_memory_growth(gpus[0], True)
  fire.Fire(evaluate)
